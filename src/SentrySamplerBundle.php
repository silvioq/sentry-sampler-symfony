<?php

namespace Silvioq\SentrySamplerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SentrySamplerBundle extends Bundle
{
    public function build(ContainerBuilder $container) : void
    {
        parent::build($container);
    }
}
