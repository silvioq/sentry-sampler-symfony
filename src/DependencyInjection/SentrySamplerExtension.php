<?php

namespace Silvioq\SentrySamplerBundle\DependencyInjection;

use Silvioq\SentrySamplerBundle\EventListener\RequestListener;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SentrySamplerExtension extends Extension
{
    /**
     * {@inheritDoc}
     *
     * @param array<string, array<mixed>|string> $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($config["send_sample_rate"] == 0){
            return;
        }

        $definition = $container->register(RequestListener::class)
            ->addArgument(new Reference('Sentry\State\HubInterface'))
            ->addArgument($config["send_sample_rate"])
            ;

        if ($config["send_on_exception"]) {
            $definition->addMethodCall('setSendOnException', [$config["send_on_exception"]]);
        }

        $events = [
            KernelEvents::REQUEST => 'onKernelRequest',
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE => 'onKernelResponse',
            KernelEvents::VIEW => 'onKernelView',
            KernelEvents::FINISH_REQUEST => 'onKernelFinishRequest',
            KernelEvents::TERMINATE => 'onKernelTerminate',
            KernelEvents::EXCEPTION => 'onKernelException',
        ];

        foreach ($events as $event => $method) {
            $definition->addTag('kernel.event_listener', ["event" => $event, "method" => $method]);
        }
    }
}
