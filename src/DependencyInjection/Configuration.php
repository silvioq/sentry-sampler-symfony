<?php

namespace Silvioq\SentrySamplerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sentry_sampler');

        /* @phpstan-ignore-next-line */
        $treeBuilder->getRootNode()
            ->children()
                ->floatNode('send_sample_rate')
                    ->min(0.0)->max(1.0)
                    ->info('Ratio for send samples to sentry server. Set to zero for disable extension.')
                    ->defaultValue(0.01)
                ->end()
                ->booleanNode('send_on_exception')
                    ->info('Send sample in a exception.')
                    ->defaultValue(false)
                ->end()
            ->end()
            ;

        return $treeBuilder;
    }
}
