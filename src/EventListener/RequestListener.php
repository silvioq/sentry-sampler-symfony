<?php

namespace Silvioq\SentrySamplerBundle\EventListener;

use Sentry\State\HubInterface;
use Sentry\Breadcrumb;
use Sentry\Severity;
use Sentry\Event;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class RequestListener
 */
final class RequestListener
{
    /** @var HubInterface */
    private $hub;

    /** @var bool */
    private $eligible;

    /** @var Breadcrumb[] */
    private $breadcrumbs = [];

    /** @var float */
    private $start;

    /** @var bool */
    private $sendOnException = false;

    const LEVEL = Severity::INFO;
    const LOGGER = 'silvioq_sentry_sampler';

    /**
     * RequestListener constructor.
     *
     * @param HubInterface $hub
     */
    public function __construct(HubInterface $hub, float $sampleRatio) {
        $this->hub = $hub;
        $this->eligible = mt_rand(0, 99) < ($sampleRatio * 100);

        if ($this->eligible) {
            $this->addBreadcrumb('start');
        }
    }

    public function setSendOnException(bool $send) : void
    {
        $this->sendOnException = $send;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('request');
    }

    public function onKernelController(ControllerEvent $event): void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('controller');
    }

    public function onKernelResponse(ResponseEvent $event) : void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('response');
    }

    public function onKernelView(ViewEvent $event) : void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('view');
    }

    public function onKernelFinishRequest(FinishRequestEvent $event) : void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('finish_request');
    }

    public function onKernelException(ExceptionEvent $event) : void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        if (!$this->sendOnException) {
            $this->eligible = false;
        } else {
            $this->addBreadcrumb('exception');
        }
    }

    public function onKernelTerminate(TerminateEvent $event) : void
    {
        if (!$this->eligible) {
            return;
        }

        if (!$event->isMasterRequest()) {
            return;
        }

        $this->addBreadcrumb('terminate');

        foreach ($this->breadcrumbs as $breadcrumb) {
            $this->hub->addBreadcrumb($breadcrumb);
        }

        $event = Event::createEvent();
        $event->setLevel(new Severity(self::LEVEL));
        $event->setLogger(self::LOGGER);

        $this->hub->captureEvent($event);
    }

    private function addBreadcrumb(string $type) : void
    {
        $breadcrumb = new Breadcrumb(Breadcrumb::LEVEL_DEBUG, Breadcrumb::TYPE_DEFAULT, 'kernel.'. $type);

        if (!$this->start) {
            $this->start = $breadcrumb->getTimestamp();
            $time = 0.0;
        } else {
            $time = \microtime(true) - $this->start;
        }

        $this->breadcrumbs[] = $breadcrumb->withMetadata('time', $time);
    }
}
