<?php

namespace Silvioq\SentrySamplerBundle\Test\DependencyInjection;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\SentrySamplerBundle\DependencyInjection\SentrySamplerExtension;

class SentrySamplerExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions(): array
    {
        return [
            new SentrySamplerExtension(),
        ];
    }

    public function testLoadDefault()
    {
        $this->load();
        $this->compile();

        $this->assertContainerBuilderHasService('Silvioq\SentrySamplerBundle\EventListener\RequestListener');
        $this->assertContainerBuilderHasServiceDefinitionWithArgument('Silvioq\SentrySamplerBundle\EventListener\RequestListener', 1, 0.01);
    }

    public function testNotLoad()
    {
        $this->load([
            'send_sample_rate' => 0.0
        ]);
        $this->compile();

        $this->assertContainerBuilderNotHasService('Silvioq\SentrySamplerBundle\EventListener\RequestListener');
    }

    public function testWithArgs()
    {
        $this->load([
            'send_sample_rate' => 0.2,
            "send_on_exception" => true
        ]);
        $this->compile();

        $this->assertContainerBuilderHasService('Silvioq\SentrySamplerBundle\EventListener\RequestListener');
        $this->assertContainerBuilderHasServiceDefinitionWithArgument('Silvioq\SentrySamplerBundle\EventListener\RequestListener', 1, 0.2);
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall('Silvioq\SentrySamplerBundle\EventListener\RequestListener', 'setSendOnException', [true]);
    }
}
