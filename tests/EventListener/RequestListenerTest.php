<?php

namespace Silvioq\SentrySamplerBundle\Test\EventListener;

use PHPUnit\Framework\TestCase;
use Sentry\State\HubInterface;
use Silvioq\SentrySamplerBundle\EventListener\RequestListener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestListenerTest extends TestCase
{
    /**
     * @var HubInterface
     */
    private $hub;

    /**
     * @var HttpKernelInterface
     */
    private $kernel;

    public function setUp() : void
    {
        $this->hub = $this->getMockBuilder(HubInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->kernel = $this->getMockBuilder(HttpKernelInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testNoEligible()
    {
        $this->hub->expects($this->never())
            ->method('addBreadcrumb')
            ;
        $this->hub->expects($this->never())
            ->method('captureEvent')
            ;

        $request = new RequestListener($this->hub, -1.0);
        $request->setSendOnException(true);

        $event = new RequestEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);
        $request->onKernelRequest($event);

        $event = new ControllerEvent($this->kernel, function() { return true; }, new Request(), HttpKernelInterface::MASTER_REQUEST);
        $request->onKernelController($event);

        $event = new ResponseEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, new Response());
        $request->onKernelResponse($event);

        $event = new ViewEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, '');
        $request->onKernelView($event);
    
        $event = new ExceptionEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, new \RuntimeException('test'));
        $request->onKernelException($event);

        $event = new TerminateEvent($this->kernel, new Request(), new Response());
        $request->onKernelTerminate($event);
    }

    public function testEligible()
    {
        $this->hub->expects($this->exactly(6))
            ->method('addBreadcrumb')
            ;
        $this->hub->expects($this->once())
            ->method('captureEvent')
            ->with($this->isInstanceOf(\Sentry\Event::class))
            ;

        $request = new RequestListener($this->hub, 1.0);

        $event = new RequestEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);
        $request->onKernelRequest($event);

        $event = new ControllerEvent($this->kernel, function() { return true; }, new Request(), HttpKernelInterface::MASTER_REQUEST);
        $request->onKernelController($event);

        $event = new ResponseEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, new Response());
        $request->onKernelResponse($event);

        $event = new ViewEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, '');
        $request->onKernelView($event);
    
        $event = new TerminateEvent($this->kernel, new Request(), new Response());
        $request->onKernelTerminate($event);
    }

    public function testException()
    {
        $this->hub->expects($this->never())
            ->method('addBreadcrumb')
            ;
        $this->hub->expects($this->never())
            ->method('captureEvent')
            ;

        $request = new RequestListener($this->hub, 1.0);

        $event = new RequestEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);
        $request->onKernelRequest($event);

        $event = new ExceptionEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, new \RuntimeException('test'));
        $request->onKernelException($event);

        $event = new TerminateEvent($this->kernel, new Request(), new Response());
        $request->onKernelTerminate($event);

    }
    public function testExceptionWithSend()
    {
        $this->hub->expects($this->exactly(3))
            ->method('addBreadcrumb')
            ;
        $this->hub->expects($this->once())
            ->method('captureEvent')
            ;

        $request = new RequestListener($this->hub, 1.0);
        $request->setSendOnException(true);

        $event = new ExceptionEvent($this->kernel, new Request(), HttpKernelInterface::MASTER_REQUEST, new \RuntimeException('test'));
        $request->onKernelException($event);

        $event = new TerminateEvent($this->kernel, new Request(), new Response());
        $request->onKernelTerminate($event);
    }
}
